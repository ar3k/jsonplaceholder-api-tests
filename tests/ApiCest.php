<?php
class ApiCest
{
    public function getPosts(ApiTester $I){
        $I->sendGET('/posts');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $givenResponse  = $I->grabResponse();

        for($i=0;$i<100;$i++){
            $startOfPath = '$[' . $i . ']';
            $properties = array(
                $startOfPath.".userId",
                $startOfPath.".id",
                $startOfPath.".title",
                $startOfPath.".body"
            );

            $I->seeResponseJsonMatchesJsonPath($properties[0],$properties[1],$properties[2],$properties[3]);
        }

        $I->seeResponseMatchesJsonType([
            'userId' => 'integer',
            'id' => 'integer',
            'title' => 'string',
            'body' => 'string'
        ]);

    }

    public function postPost(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type','application/json','charset=UTF-8');
        $I->sendPOST('/posts',['title' => 'foo', 'body' => 'bar','userId' => 10]);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();

        $givenResponse = $I->grabResponse();
        var_dump($givenResponse);

        $I->seeResponseContainsJson([
            'userId' => 10,
            'title' => 'foo',
            'body' => 'bar'
        ]);
    }

    public function getPost(ApiTester $I){
        $I->sendGET('/posts/1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
                'userId' => 1,
                'id' => 1,
                'title' => 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
                'body' => "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            ]
        );

        $I->seeResponseJsonMatchesJsonPath('$.userId','$.id','$.title','$.body');

        $I->seeResponseMatchesJsonType([
            'userId' => 'integer',
            'id' => 'integer',
            'title' => 'string',
            'body' => 'string'
        ]);

        $givenResponse = $I->grabResponse();
        var_dump($givenResponse);
    }

    public function deletePost(ApiTester $I){
        $I->sendDELETE('/posts/100');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $givenResponse = $I->grabResponse();
        var_dump($givenResponse);
    }

    public function putPost(ApiTester $I){
        $I->sendPUT('/posts/100',['userId' => '9','title'=>'Mariusz Pudzianowski','body' => 'Jest boski']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $givenResponse = $I->grabResponse();
        var_dump($givenResponse);
    }

    public function patchPost(ApiTester $I){
        /*$I->sendPATCH('/posts/100',[
            'userId' => 1,
            'title' => 'Mariusz Pudzianowski',
            'body' => "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        ]);*/

        $I->sendPATCH('/posts/100',[
            'body' => "Cokolwiek"
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $givenResponse = $I->grabResponse();
        var_dump($givenResponse);
    }
}